package de.uniks.rubikscube.model;

import org.kociemba.twophase.Search;

import java.util.HashMap;
import java.util.LinkedList;

public class CubeSolution {

    private static final HashMap<Character, CubeSideIdentifier> cubeSideMap;

    static {
        cubeSideMap = new HashMap<>(6);
        cubeSideMap.put('U', CubeSideIdentifier.TOP);
        cubeSideMap.put('D', CubeSideIdentifier.BOTTOM);
        cubeSideMap.put('L', CubeSideIdentifier.LEFT);
        cubeSideMap.put('R', CubeSideIdentifier.RIGHT);
        cubeSideMap.put('F', CubeSideIdentifier.FRONT);
        cubeSideMap.put('B', CubeSideIdentifier.BACK);
    }

    private final String solutionString;
    private LinkedList<CubeMove> cubeMoves;

    CubeSolution(String cubeString, int timeout) throws CubeNotSolvableException {
        solutionString = Search.solution(cubeString, 21, timeout, false);
        if (solutionString.startsWith("Error")) {
            int error = Integer.parseInt("" + solutionString.charAt(6));
            switch (error) {
                case 1:
                    throw new CubeNotSolvableException("There is not exactly one facelet of each colour!");
                case 2:
                    throw new CubeNotSolvableException("Not all 12 edges exist exactly once!");
                case 3:
                    throw new CubeNotSolvableException("Flip error: One edge has to be flipped!");
                case 4:
                    throw new CubeNotSolvableException("Not all corners exist exactly once!");
                case 5:
                    throw new CubeNotSolvableException("Twist error: One corner has to be twisted!");
                case 6:
                    throw new CubeNotSolvableException("Parity error: Two corners or two edges have to be exchanged");
                case 7:
                    throw new CubeNotSolvableException("No solution exists for the given maxDepth (this should never happen)!");
                case 8:
                    throw new CubeNotSolvableException("Timeout, no solution within given time");
                default:
                    throw new CubeNotSolvableException("Unknown error");
            }
        } else {
            cubeMoves = new LinkedList<>();
            for (String move : solutionString.split(" ")) {
                CubeSideIdentifier cubeSide = cubeSideMap.get(move.charAt(0));
                CubeMove.Direction direction;
                boolean moveTwoTimes = false;

                if (move.length() == 1) {
                    direction = CubeMove.Direction.CLOCKWISE;
                } else if (move.charAt(1) == '\'') {
                    direction = CubeMove.Direction.COUNTERCLOCKWISE;
                } else {
                    direction = CubeMove.Direction.CLOCKWISE;
                    moveTwoTimes = true;
                }

                CubeMove cubeMove = new CubeMove(cubeSide, direction);

                if (moveTwoTimes) {
                    cubeMoves.add(cubeMove);
                }

                cubeMoves.add(cubeMove);
            }
        }
    }

    public String getSolutionString() {
        return solutionString;
    }

    public LinkedList<CubeMove> getCubeMoves() {
        return cubeMoves;
    }
}
