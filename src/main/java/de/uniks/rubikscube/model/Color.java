package de.uniks.rubikscube.model;

import java.util.TreeMap;

public class Color {

    /**
     * Used to map hue values to color names.
     */
    private static TreeMap<Integer, Range> hueMap;

    static {
        hueMap = new TreeMap<>();
        hueMap.put(0, Range.RED);
        hueMap.put(15, Range.ORANGE);
        hueMap.put(40, Range.YELLOW);
        hueMap.put(70, Range.GREEN);
        hueMap.put(160, Range.CYAN);
        hueMap.put(200, Range.BLUE);
        hueMap.put(270, Range.MAGENTA);
        hueMap.put(330, Range.RED);
    }

    private double r;
    private double g;
    private double b;

    /**
     * @param r Red color value (0.0 - 1.0)
     * @param g Green color value (0.0 - 1.0)
     * @param b Blue color value (0.0 - 1.0)
     */
    Color(double r, double g, double b) {
        setR(r);
        setG(g);
        setB(b);
    }

    /**
     * Used to init object with given HEX string.
     *
     * @param color HEX string like 0xAABBCCDD, where AA represents red, BB green, CC blue and DD alpha.
     *              Alpha is ignored.
     * @throws IllegalArgumentException when {@code color} parameter is malformed.
     */
    Color(String color) {
        if (color == null
                || color.length() != 10
                || !color.startsWith("0x")) {
            throw new IllegalArgumentException();
        }

        setR(Integer.decode("0x" + color.substring(2, 4)) * (1 / 256d));
        setG(Integer.decode("0x" + color.substring(4, 6)) * (1 / 256d));
        setB(Integer.decode("0x" + color.substring(6, 8)) * (1 / 256d));
    }

    public double getR() {
        return r;
    }

    public void setR(double r) {
        if (r > 1.0) {
            this.r = 1.0;
        } else if (r < 0.0) {
            this.r = 0.0;
        } else {
            this.r = r;
        }
    }

    public double getG() {
        return g;
    }

    public void setG(double g) {
        if (g > 1.0) {
            this.g = 1.0;
        } else if (g < 0.0) {
            this.g = 0.0;
        } else {
            this.g = g;
        }
    }

    public double getB() {
        return b;
    }

    public void setB(double b) {
        if (b > 1.0) {
            this.b = 1.0;
        } else if (b < 0.0) {
            this.b = 0.0;
        } else {
            this.b = b;
        }
    }

    public void setRGB(double r, double g, double b) {
        setR(r);
        setG(g);
        setB(b);
    }

    /**
     * Calculates hue value.
     *
     * @return hue value (0.0 - 360.0)
     * @see <a href="https://de.wikipedia.org/wiki/HSV-Farbraum#Umrechnung_RGB_in_HSV/HSL">Wikipedia</a>
     */
    private double getHue() {

        double max = Math.max(r, Math.max(g, b));
        double min = Math.min(r, Math.min(g, b));

        double result = 0d;

        if (max == r) {
            result = 60 * ((g - b) / (max - min));
        } else if (max == g) {
            result = 60 * (2 + ((b - r) / (max - min)));
        } else if (max == b) {
            result = 60 * (4 + ((r - g) / (max - min)));
        }

        return (result < 0 ? result + 360 : result);
    }

    /**
     * Calculates saturation value.
     *
     * @return saturation value (0.0 - 1.0)
     * @see <a href="https://de.wikipedia.org/wiki/HSV-Farbraum#Umrechnung_RGB_in_HSV/HSL">Wikipedia</a>
     */
    private double getSaturation() {
        double max = Math.max(r, Math.max(g, b));
        if (max == 0) {
            return 0;
        } else {
            double min = Math.min(r, Math.min(g, b));
            return (max - min) / max;
        }
    }

    /**
     * Calculates brightness value.
     *
     * @return brightness value (0.0 - 1.0)
     * @see <a href="https://de.wikipedia.org/wiki/HSV-Farbraum#Umrechnung_RGB_in_HSV/HSL">Wikipedia</a>
     */
    private double getBrightness() {
        return Math.max(r, Math.max(g, b));
    }

    /**
     * Determines which color we see.
     *
     * @return Color range from {@link Range}
     */
    public Range getRange() {
        if (getBrightness() < 0.5) {
            return Range.BLACK;
        } else if (getSaturation() < 0.3) {
            return Range.WHITE;
        } else {
            return hueMap.floorEntry((int) Math.floor(getHue())).getValue();
        }
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        } else if (other == this) {
            return true;
        } else if (!(other instanceof Color)) {
            return false;
        } else {
            Color otherColor = (Color) other;
            return r == otherColor.getR()
                    && g == otherColor.getG()
                    && b == otherColor.getB();
        }
    }

    public enum Range {
        RED, ORANGE, YELLOW, GREEN, CYAN, BLUE, MAGENTA, BLACK, WHITE
    }
}
