package de.uniks.rubikscube.model;

public class CouldNotDetermineSideColorsException extends Exception {

    CouldNotDetermineSideColorsException() {
        super("Could not determine side colors. Check your probe points!");
    }
}
