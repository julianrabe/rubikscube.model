package de.uniks.rubikscube.model;

import java.util.Collection;
import java.util.HashMap;
import java.util.StringJoiner;

public class Cube {

    private boolean modelWasUpdated;
    private HashMap<Color.Range, CubeSideIdentifier> colorSideMap;
    private HashMap<CubeSideIdentifier, CubeSide> cubeSides;

    public Cube() {
        modelWasUpdated = false;
        cubeSides = new HashMap<>(6);
        for (CubeSideIdentifier id : CubeSideIdentifier.values()) {
            cubeSides.put(id, new CubeSide(id));
        }
    }

    /**
     * Determine the image we want to read from (left or right).
     *
     * @param leftImage  Left image, responsible for Top, Left and Front.
     * @param rightImage Right image, responsible for Right, Back and Bottom.
     * @param id         Identifier of the current side.
     * @return The right PixelReader for this side of the cube.
     */
    private static SimpleImage getImage(SimpleImage leftImage, SimpleImage rightImage, CubeSideIdentifier id) {
        switch (id) {
            case TOP:
            case LEFT:
            case FRONT:
                return leftImage;
            default:
                return rightImage;
        }
    }

    /**
     * Determines facelet colors and prints them to stdout.
     *
     * @param leftImage  Left image, responsible for Top, Left and Front.
     * @param rightImage Right image, responsible for Right, Back and Bottom.
     */
    public void printToConsole(SimpleImage leftImage, SimpleImage rightImage) {

        StringJoiner stringJoiner = new StringJoiner("\n\n");

        for (CubeSide cubeSide : cubeSides.values()) {

            StringJoiner subJoiner = new StringJoiner("\n");
            CubeSideIdentifier id = cubeSide.getId();

            for (int i = 0; i < cubeSide.getFacelets().size(); i++) {

                Facelet facelet = cubeSide.getFacelet(i);

                SimpleImage tmpImage = getImage(leftImage, rightImage, id);
                XY imageCoordinates = facelet.getImageCoordinates();

                String color = tmpImage.getColor(imageCoordinates.getX(), imageCoordinates.getY());

                subJoiner.add("Side: " + id.name() + " | Facelet#" + i + " | "
                        + "Pixel " + imageCoordinates.getX() + "/" + imageCoordinates.getY() + ":  \t"
                        + color + "\t-> "
                        + new Color(color).getRange().name());
            }

            stringJoiner.add(subJoiner.toString());
        }

        System.out.println(stringJoiner.toString());
    }

    /**
     * Determines facelet colors and writes them to the model, potentially firing bean notifications.
     *
     * @param leftImage  Left image, responsible for Top, Left and Front.
     * @param rightImage Right image, responsible for Right, Back and Bottom.
     */
    public void updateModel(SimpleImage leftImage, SimpleImage rightImage) throws CouldNotDetermineSideColorsException {

        colorSideMap = new HashMap<>(6);

        for (CubeSide cubeSide : cubeSides.values()) {

            CubeSideIdentifier id = cubeSide.getId();

            for (int i = 0; i < 9; i++) {

                Facelet facelet = cubeSide.getFacelet(i);
                SimpleImage tmpImage = getImage(leftImage, rightImage, id);

                facelet.setColor(tmpImage.getColor(facelet.getImageCoordinates().getX(), facelet.getImageCoordinates().getY()));
            }

            if(colorSideMap.put(cubeSide.getPrimaryFacelet().getColor().getRange(), cubeSide.getId()) != null) {
                throw new CouldNotDetermineSideColorsException();
            }
        }

        modelWasUpdated = true;
    }

    /**
     * Computes the cube representation string according to Herbert Kociemba's solver.
     *
     * @return cube representation
     * @throws ModelNeverUpdatedException when model was never updated and therefore never filled with colors.
     */
    public String getKociembaCubeString() throws ModelNeverUpdatedException {
        if (modelWasUpdated) {
            return cubeSides.get(CubeSideIdentifier.TOP).getKociembaCubeSideString(colorSideMap) +
                    cubeSides.get(CubeSideIdentifier.RIGHT).getKociembaCubeSideString(colorSideMap) +
                    cubeSides.get(CubeSideIdentifier.FRONT).getKociembaCubeSideString(colorSideMap) +
                    cubeSides.get(CubeSideIdentifier.BOTTOM).getKociembaCubeSideString(colorSideMap) +
                    cubeSides.get(CubeSideIdentifier.LEFT).getKociembaCubeSideString(colorSideMap) +
                    cubeSides.get(CubeSideIdentifier.BACK).getKociembaCubeSideString(colorSideMap);
        } else {
            throw new ModelNeverUpdatedException();
        }
    }

    /**
     * @param id The CubeSideIdentifier for the CubeSide to return.
     * @return CubeSide matching given identifier
     */
    public CubeSide getCubeSide(CubeSideIdentifier id) {
        return cubeSides.get(id);
    }

    public Collection<CubeSide> getCubeSides() {
        return cubeSides.values();
    }

    public CubeSolution getSolution(int timeout) throws ModelNeverUpdatedException, CubeNotSolvableException {
        return new CubeSolution(getKociembaCubeString(), timeout);
    }
}
