package de.uniks.rubikscube.model;

import java.util.ArrayList;
import java.util.HashMap;

public class CubeSide {

    private ArrayList<Facelet> facelets;
    private CubeSideIdentifier id;
    private Facelet primaryFacelet;

    CubeSide(CubeSideIdentifier id) {
        this.id = id;
        facelets = new ArrayList<>(9);

        for (int i = 0; i < 9; i++) {
            facelets.add(new Facelet());
        }

        primaryFacelet = facelets.get(4);
    }

    /**
     * Computes the cube representation string according to Herbert Kociemba's solver only for one side of the cube.
     *
     * @param colorSideMap Mapping from color to sides.
     * @return cube representation substring.
     */
    String getKociembaCubeSideString(HashMap<Color.Range, CubeSideIdentifier> colorSideMap) {
        StringBuilder result = new StringBuilder();
        for (Facelet facelet : facelets) {
            switch (colorSideMap.get(facelet.getColor().getRange())) {
                case TOP:
                    result.append("U");
                    break;
                case BOTTOM:
                    result.append("D");
                    break;
                case FRONT:
                    result.append("F");
                    break;
                case BACK:
                    result.append("B");
                    break;
                case LEFT:
                    result.append("L");
                    break;
                case RIGHT:
                    result.append("R");
                    break;
            }
        }
        return result.toString();
    }

    public ArrayList<Facelet> getFacelets() {
        return facelets;
    }

    public Facelet getFacelet(int index) {
        return facelets.get(index);
    }

    public CubeSideIdentifier getId() {
        return id;
    }

    public Facelet getPrimaryFacelet() {
        return primaryFacelet;
    }
}
