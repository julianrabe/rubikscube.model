package de.uniks.rubikscube.model;

public interface SimpleImage {

    String getColor(int x, int y);
}
