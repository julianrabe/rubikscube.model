package de.uniks.rubikscube.model;

public class ModelNeverUpdatedException extends Exception {

    ModelNeverUpdatedException() {
        super("Model was never updated!");
    }
}
