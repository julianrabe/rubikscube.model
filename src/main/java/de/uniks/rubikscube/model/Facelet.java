package de.uniks.rubikscube.model;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

public class Facelet {

    public static final String PROPERTY_COLOR = "color";
    public static final String PROPERTY_IMAGE_COORDINATES = "imageCoordinates";

    /**
     * HEX color string
     */
    private Color color;
    private XY imageCoordinates;
    private PropertyChangeSupport changeSupport;

    Facelet() {
        changeSupport = new PropertyChangeSupport(this);
    }

    Facelet(XY imageCoordinates) {
        this();
        this.imageCoordinates = imageCoordinates;
    }

    public Color getColor() {
        return color;
    }

    /**
     * Sets the color and fires a PropertyChange when value changed.
     *
     * @param color The new color
     * @return true if value changed
     */
    public boolean setColor(String color) {
        Color oldColor = this.color;
        Color newColor = null;
        boolean changed = false;

        if (color != null) {
            newColor = new Color(color);
        }

        if (newColor != null && !newColor.equals(oldColor)) {
            changed = true;
        } else if (oldColor != null) {
            changed = true;
        }

        if (changed) {
            this.color = newColor;
            changeSupport.firePropertyChange(PROPERTY_COLOR, oldColor, newColor);
        }

        return changed;
    }

    public XY getImageCoordinates() {
        return imageCoordinates;
    }

    public void setImageCoordinates(XY imageCoordinates) {
        if (this.imageCoordinates != imageCoordinates) {
            XY oldCoordinates = this.imageCoordinates;
            this.imageCoordinates = imageCoordinates;
            changeSupport.firePropertyChange(PROPERTY_IMAGE_COORDINATES, oldCoordinates, imageCoordinates);
        }
    }

    public void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(propertyName, listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
}
