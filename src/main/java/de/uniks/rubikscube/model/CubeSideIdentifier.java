package de.uniks.rubikscube.model;

/**
 * Make sure we always talk about the same side of the world :)
 */
public enum CubeSideIdentifier {
    TOP, BOTTOM, LEFT, RIGHT, FRONT, BACK
}
