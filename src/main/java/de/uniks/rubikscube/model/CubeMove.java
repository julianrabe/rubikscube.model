package de.uniks.rubikscube.model;

public class CubeMove {

    private final CubeSideIdentifier cubeSide;
    private final Direction direction;

    public CubeMove(CubeSideIdentifier cubeSide, Direction direction) {
        this.cubeSide = cubeSide;
        this.direction = direction;
    }

    public CubeSideIdentifier getCubeSide() {
        return cubeSide;
    }

    public Direction getDirection() {
        return direction;
    }

    public enum Direction {
        CLOCKWISE, COUNTERCLOCKWISE
    }
}
