package de.uniks.rubikscube.model;

public class CubeNotSolvableException extends Exception {

    CubeNotSolvableException(String message) {
        super(message);
    }
}
