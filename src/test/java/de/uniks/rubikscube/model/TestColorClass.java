package de.uniks.rubikscube.model;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestColorClass {

    @Test
    public void testColorRanges() {

        // Trivial colors:

        Color testColor = new Color(0, 0, 0);
        assertEquals(Color.Range.BLACK, testColor.getRange());

        testColor.setRGB(0, 0, 1);
        assertEquals(Color.Range.BLUE, testColor.getRange());

        testColor.setRGB(0, 1, 0);
        assertEquals(Color.Range.GREEN, testColor.getRange());

        testColor.setRGB(0, 1, 1);
        assertEquals(Color.Range.CYAN, testColor.getRange());

        testColor.setRGB(1, 0, 0);
        assertEquals(Color.Range.RED, testColor.getRange());

        testColor.setRGB(1, 0, 1);
        assertEquals(Color.Range.MAGENTA, testColor.getRange());

        testColor.setRGB(1, 1, 0);
        assertEquals(Color.Range.YELLOW, testColor.getRange());

        testColor.setRGB(1, 1, 1);
        assertEquals(Color.Range.WHITE, testColor.getRange());

        // Complex colors:

        testColor.setRGB(1, 0.2, 0);
        assertEquals(Color.Range.RED, testColor.getRange());

        testColor.setRGB(1, 0.4, 0);
        assertEquals(Color.Range.ORANGE, testColor.getRange());

        testColor.setRGB(1, 0.6, 0);
        assertEquals(Color.Range.ORANGE, testColor.getRange());

        testColor.setRGB(1, 0.8, 0);
        assertEquals(Color.Range.YELLOW, testColor.getRange());

        testColor.setRGB(0.78, 1, 0);
        assertEquals(Color.Range.GREEN, testColor.getRange());

        testColor.setRGB(0, 1, 0.72);
        assertEquals(Color.Range.CYAN, testColor.getRange());

        testColor.setRGB(0, 0.86, 1);
        assertEquals(Color.Range.CYAN, testColor.getRange());

        testColor.setRGB(0.58, 0, 1);
        assertEquals(Color.Range.MAGENTA, testColor.getRange());

        testColor.setRGB(1, 0, 0.5);
        assertEquals(Color.Range.RED, testColor.getRange());
    }
}
